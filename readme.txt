Desafio DBServer

Fluxo:

Acesso ao sistema de compra
Realizado pesquisa por produto passado por parâmetro
Selecionado um dos produtos (aleatório) exibidos na tela de resultado
Produto inserido no carrinho
Realizado pesquisa por outro produto passado por parâmetro
Selecionado um dos produtos (aleatório) exibidos na tela de resultado
Produto inserido no carrinho
Acesso ao carrinho de compras e realizado validação se os mesmos produtos inseridos constam no carrinho
Gerado registro da execução em formato txt