package br.com.testedbserver.automacao.ct;

import org.junit.Test;
import br.com.testedbserver.automacao.apoio.TesteBase;
import br.com.testedbserver.automacao.telas.metodos.TelaResultadoPesquisaMetodos;
import junit.framework.TestCase;
import br.com.testedbserver.automacao.telas.metodos.TelaCarrinhoCompraMetodos;
import br.com.testedbserver.automacao.telas.metodos.TelaDescricaoProdutoMetodos;
import br.com.testedbserver.automacao.telas.metodos.TelaInicialMetodos;


/**
 * Caso de teste para inclusão de produtos no carrinho e validação
 * @author Ricardo Kohatsu
 *
 */
public class CT001 extends TesteBase{
	@Test
	public void test() {
		try{
			TelaInicialMetodos telaInicial = new TelaInicialMetodos();
			TelaResultadoPesquisaMetodos telaResultado = new TelaResultadoPesquisaMetodos();
			TelaDescricaoProdutoMetodos telaProduto = new TelaDescricaoProdutoMetodos();
			TelaCarrinhoCompraMetodos telaCarrinho = new TelaCarrinhoCompraMetodos();
			
			telaInicial.realizarPesquisaDeProduto("Batman");
			telaResultado.selecionarProduto();
			telaProduto.adicionarProdutoAoCarrinho();
			telaInicial.realizarPesquisaDeProduto("PS3");
			telaResultado.selecionarProduto();
			telaProduto.adicionarProdutoAoCarrinho();
			telaInicial.acessarCarrinhoDeCompra();
			telaCarrinho.validarItensCarrinho();
		}

		catch (Exception e) {
			TesteBase.registrarDados("Execução falhou: " + e);
			TestCase.fail();
		}
	}
}
