package br.com.testedbserver.automacao.apoio;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável por gerenciar itens inseridos no carrinho
 * @author Ricardo Kohatsu
 *
 */
public class CarrinhoCompras {

	public static List<String> listaItensCarrinho = new ArrayList<String>();
	
	public static void AdicionarItem(String nomeItem) {
		listaItensCarrinho.add(nomeItem);
	}
	
	public static List getListaItens() {
		return listaItensCarrinho;
	}	
	
}
