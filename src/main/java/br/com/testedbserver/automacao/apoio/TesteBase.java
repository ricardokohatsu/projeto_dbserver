package br.com.testedbserver.automacao.apoio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import junit.framework.TestCase;
import junit.framework.TestResult;

public class TesteBase {

	public static WebDriver driver;
	private static ArquivoRegistro arquivo;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	
	@Before
	public void before() {
		registrarDados("Execução iniciada: " + dateFormat.format(date));
		Navegador navegador = new Navegador();
		navegador.setNomeNavegador("Chrome");
		driver = navegador.configurarNavegador();
		driver.manage().window().maximize();
		navegador.acessarPaginaWeb("http://demo.cs-cart.com/", driver);
	}	
	
	@After
	public void after() {
		driver.close();
		registrarDados("Execução Finalizada: " + dateFormat.format(date));
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
	
	
	public static void registrarDados(String dados){
		arquivo = new ArquivoRegistro();
		String nomeArquivo = "registro_execucao.txt";
		String diretorioArquivo = "C:\\Users\\Public\\RegistroExecucao";
		arquivo.gerarArquivo(diretorioArquivo, nomeArquivo, dados);
	}
	
}
