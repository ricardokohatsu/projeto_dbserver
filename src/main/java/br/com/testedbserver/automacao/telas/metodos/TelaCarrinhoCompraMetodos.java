package br.com.testedbserver.automacao.telas.metodos;

import static org.junit.Assert.assertEquals;

import java.util.List;
import org.openqa.selenium.support.PageFactory;
import br.com.testedbserver.automacao.telas.mapeamento.TelaCarrinhoCompra;
import br.com.testedbserver.automacao.apoio.TesteBase;
import br.com.testedbserver.automacao.apoio.CarrinhoCompras;

public class TelaCarrinhoCompraMetodos extends TelaCarrinhoCompra{
	
	public TelaCarrinhoCompraMetodos() {
		PageFactory.initElements(TesteBase.getDriver(), this);
	}
	
	public void validarItensCarrinho() {
		List<String> listaItensAdicionados = CarrinhoCompras.getListaItens();
		assertEquals(listaItensAdicionados.size(), listaProdutosCarrinho.size());
		for (int contador = 0; contador < listaItensAdicionados.size(); contador++) {
			assertEquals(true, listaItensAdicionados.contains(listaProdutosCarrinho.get(contador).getText()));
			TesteBase.registrarDados("Realizado validação da inclusão do item no carrinho: " + listaItensAdicionados.get(contador));
		}
	}

}
