package br.com.testedbserver.automacao.telas.metodos;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import br.com.testedbserver.automacao.telas.mapeamento.TelaResultadoPesquisa;
import br.com.testedbserver.automacao.apoio.TesteBase;

public class TelaResultadoPesquisaMetodos extends TelaResultadoPesquisa{
	
	public TelaResultadoPesquisaMetodos() {
		PageFactory.initElements(TesteBase.getDriver(), this);
	}
	
	public void selecionarProduto() {
		selecionarProduto(0);
	}	
	/**
	 * Seleciona o produto de acordo com o index passado por parâmetro. Caso seja 0, será selecionado um produto aleatório da tela
	 * @param numProduto
	 */
	public void selecionarProduto(int numProduto) {
		int qtdElementos = listaProdutos.size();
		if (qtdElementos > 0){
			if (numProduto == 0) {
				Random numRandomico = new Random();
				numProduto = numRandomico.nextInt(qtdElementos);
			}
			listaProdutos.get(numProduto).click();
		}
	}

}
