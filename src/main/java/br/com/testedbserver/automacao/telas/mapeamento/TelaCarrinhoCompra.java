package br.com.testedbserver.automacao.telas.mapeamento;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TelaCarrinhoCompra {

	@FindBy(className = "ty-cart-content__product-title")
	protected List<WebElement> listaProdutosCarrinho;	
}
