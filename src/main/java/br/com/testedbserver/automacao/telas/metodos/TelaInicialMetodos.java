package br.com.testedbserver.automacao.telas.metodos;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.testedbserver.automacao.telas.mapeamento.TelaInicial;
import br.com.testedbserver.automacao.apoio.TesteBase;

public class TelaInicialMetodos extends TelaInicial{
	public TelaInicialMetodos() {
		PageFactory.initElements(TesteBase.getDriver(), this);
	}
	
	public void pressionarPesquisar() {
		botaoPesquisar.click();
	}
	
	public void acessarCarrinhoDeCompra() {
		pressionarCarrinho();
		pressionarVerCarrinho();
	}

	public void pressionarCarrinho() {
		try {
			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(TesteBase.getDriver(), 10);
			wait.until(ExpectedConditions.elementToBeClickable(botaoCarrinho));
			botaoCarrinho.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void pressionarVerCarrinho() {
		WebDriverWait wait = new WebDriverWait(TesteBase.getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(botaoVerCarrinho));
		botaoVerCarrinho.click();
	}	
	
	public void realizarPesquisaDeProduto(String nomeProduto) {
		preencherCampoProcurarProduto(nomeProduto);
		pressionarPesquisar();
	}
	
	public void preencherCampoProcurarProduto(String produto) {
		campoProcurarProdutos.sendKeys(produto);
	}
	
}
