package br.com.testedbserver.automacao.telas.mapeamento;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TelaResultadoPesquisa {

	@FindBy(id = "search_input")
	protected WebElement campoProcurarProdutos;

	@FindBy(className = "product-title")
	protected List<WebElement> listaProdutos;	
}
