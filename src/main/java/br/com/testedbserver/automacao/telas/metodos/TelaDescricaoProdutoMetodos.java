package br.com.testedbserver.automacao.telas.metodos;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.testedbserver.automacao.telas.mapeamento.TelaDescricaoProduto;
import br.com.testedbserver.automacao.apoio.TesteBase;
import br.com.testedbserver.automacao.apoio.CarrinhoCompras;
public class TelaDescricaoProdutoMetodos extends TelaDescricaoProduto{
	
	public TelaDescricaoProdutoMetodos() {
		PageFactory.initElements(TesteBase.getDriver(), this);
	}
	
	public void clicarAdicionarAoCarrinho() {
		botaoAdicionar.click();
	}
	
	public String getNomeProduto() {
		return txtNomeProduto.getText();
	}

	public void adicionarProdutoAoCarrinho() {
		CarrinhoCompras.AdicionarItem(getNomeProduto());
		clicarAdicionarAoCarrinho();
		clicarContinuarCompras();
	}
	
	public void clicarContinuarCompras(){
		WebDriverWait wait = new WebDriverWait(TesteBase.getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(botaoContinuarCompras));
		botaoContinuarCompras.click();

	}
}
