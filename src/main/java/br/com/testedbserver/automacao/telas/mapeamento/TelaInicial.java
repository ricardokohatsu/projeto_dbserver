package br.com.testedbserver.automacao.telas.mapeamento;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TelaInicial {

	@FindBy(id = "search_input")
	protected WebElement campoProcurarProdutos;

	@FindBy(className = "ty-icon-search")
	protected WebElement botaoPesquisar;	
	
	@FindBy(xpath = "//I[@class = 'ty-minicart__icon ty-icon-moon-commerce filled']")
	protected WebElement botaoCarrinho;
	
	
	@FindBy(xpath = "//A[@class = 'ty-btn ty-btn__secondary']")
	protected WebElement botaoVerCarrinho;		
	
}
