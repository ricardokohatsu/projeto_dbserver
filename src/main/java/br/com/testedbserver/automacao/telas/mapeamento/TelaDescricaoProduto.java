package br.com.testedbserver.automacao.telas.mapeamento;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TelaDescricaoProduto {
	
	@FindBy(xpath = "//BUTTON[@class = 'ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn']")
	protected WebElement botaoAdicionar;	
	
	@FindBy(xpath = "//a[@class = 'ty-btn ty-btn__secondary cm-notification-close ']")
	protected WebElement botaoContinuarCompras;	

	@FindBy(className = "ty-product-block-title")
	protected WebElement txtNomeProduto;	
	
	
}
